var mqttConnect = require('mqtt/lib/connect');
var connStr = null;
var rootTopic = null;
var client = null;
// client.subscribe(rootTopic);
var state = {

}

// client.on("message", function (topic, payload) {
//   console.warn('message->', [topic, payload].join(": "))
//   // client.end();
// })
function onError (e){
    console.error('comm module: ', e)
}

function openConn(aConnStr, aRootTopic) {
    connStr = aConnStr;
    rootTopic = aRootTopic;
    if (connStr && rootTopic) {
        client = mqttConnect(connStr) // you add a ws:// url here
        client.on('error', onError);
        client.publish(rootTopic, "hello world!");    
    } else {
        throw new Error('required argument missed!')
    }
}

function closeConn() {
    var force = true;
    if(client) {
        client.publish(rootTopic, "goodbye world!");
        client.end(force);    
        client = null;
    }
}

function resumeConn() {
    client = mqtt.connect(connStr) // you add a ws:// url here
    client.on('error', onError);
    client.publish(rootTopic, "hello world again!");
}

function pauseConn() {
    if(client) {
        client.publish(rootTopic, "going pause!");
        client.end();
        client = null;
    }
}

function send(message, topic) {
    if (client) {
        topic = topic || rootTopic;
        client.publish(topic, message);
    }
}



window.addEventListener('close',    closeConn);
window.addEventListener('blur',     pauseConn);
window.addEventListener('focus',    resumeConn);

module.exports = {
    openConn: openConn,
    send: send
}

