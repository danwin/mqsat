  const path = require('path');
  const merge = require('webpack-merge');
  const common = require('./webpack.common.js');
  const CleanWebpackPlugin = require('clean-webpack-plugin');
  const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

  module.exports = merge(common, {
    optimization: {
        minimizer: [new UglifyJsPlugin({
            extractComments: 'all',

            uglifyOptions: {
                warnings: false,
                parse: {},
                compress: {},
                mangle: true, // Note `mangle.properties` is `false` by default.
                output: {
                    webkit:true, 
                    comments:false
                },
                toplevel: false,
                nameCache: null,
                ie8: false,
                keep_fnames: false,
            }            

        })]
      },

    plugins: [
        new CleanWebpackPlugin(['dist']),
        // new UglifyJsPlugin({
        //     test: /\.js($|\?)/i,
        //     sourceMap: true
        // })
    ]
  });