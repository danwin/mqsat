const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    app: './src/index.js',
    comm: './src/comm.js'
    // mqsat: './src/mqsat.js'
    // mqttws31: './vendor/mqttws31.js'
  },
  plugins: [
        // new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            title: 'Output Management'
        }),
        new HtmlWebpackPlugin({  // Also generate a test.html
          filename: 'index.html',
          template: './index.html'
        })    
  ],
  resolve: {
    alias: [
      // Vendor: path.resolve(__dirname, 'vendor/'),
      // Templates: path.resolve(__dirname, 'src/templates/')      
    ]
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
    library: '[name]',
    libraryTarget: 'umd',
    umdNamedDefine: true    
  }
};


